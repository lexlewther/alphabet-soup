package com.lewthwaite.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// Loads the input file, filling the grid and checking that 
// the search words appear
public class CLetterGrid
{

	// Height and width of the grid
	private int m_nWidth;
	private int m_nHeight;

	// Grid of letters in the puzzle
	private char[][] m_carrSearchGrid;

	private class CPair
	{

		public int m_nX;
		public int m_nY;

		public CPair(int _nX, int _nY)
		{
			m_nX = _nX;
			m_nY = _nY;
		}
	}

	// List of character positions, this will be 
	// used for the start and ending position for the searches
	private ArrayList<ArrayList<CPair>> m_arrlistCharPositions;

	// List of words to check
	private ArrayList<String> m_listSearchWords;

	// Load the file
	public boolean Load(File _fileInput)
	{
		if (_fileInput == null)
		{
			return (false);
		}

		try
		{
			// Setup the position cache
			m_arrlistCharPositions = new ArrayList<ArrayList<CPair>>();
			for (int i = 0; i < 256; i += 1)
			{
				m_arrlistCharPositions.add(new ArrayList<CPair>());
			}

			// Get the contents
			Scanner scannerInput = new Scanner(_fileInput);

			/// First line is the grid size
			String[] strarrTokens = scannerInput.nextLine().split("x");
			m_nWidth = Integer.parseInt(strarrTokens[0]);
			m_nHeight = Integer.parseInt(strarrTokens[1]);

			// Create the grid
			m_carrSearchGrid = new char[m_nWidth][m_nHeight];

			// Get the rows of characters filling the grid
			for (int nRow = 0; nRow < m_nHeight; nRow += 1)
			{
				strarrTokens = scannerInput.nextLine().split(" ");

				for (int nCol = 0; nCol < m_nWidth; nCol += 1)
				{
					// Place the char in the grid
					m_carrSearchGrid[nCol][nRow] = strarrTokens[nCol].charAt(0);
					// Save off its position 
					m_arrlistCharPositions.get(strarrTokens[nCol].charAt(0)).add(new CPair(nCol, nRow));
				}
			}

			// Load the remaining rows should be search terms 
			m_listSearchWords = new ArrayList<String>();
			try
			{
				while (true)
				{
					m_listSearchWords.add(scannerInput.nextLine());
				}
			}
			catch (Exception _oXnoMoreRows)
			{
			}

			return (true);
		}
		catch (Exception _oX)
		{
			return (false);
		}
	}

	// Checks the search terms against the grid
	public List<String> CheckSearchTerms()
	{
		ArrayList<String> arrlistMessages = new ArrayList<String>();

		try
		{
			// Loop through all search terms looking for them in the grid
			for (int i = 0; i < m_listSearchWords.size(); i += 1)
			{
				// Get the first and last character
				char charStart = m_listSearchWords.get(i).charAt(0);
				char charEnd = m_listSearchWords.get(i).charAt(m_listSearchWords.get(i).length() - 1);

				// Get the possible starting and ending positions 
				ArrayList<CPair> arrlistPossibleStarting = m_arrlistCharPositions.get(charStart);
				ArrayList<CPair> arrlistPossibleEnding = m_arrlistCharPositions.get(charEnd);
				
				if ( (arrlistPossibleStarting == null || arrlistPossibleStarting.isEmpty()) ||
					   (arrlistPossibleEnding == null || arrlistPossibleEnding.isEmpty()) )
				{
					// Invalid character not appearing in the grid
					continue;
				}

				// Loop through the all possible combinations of starting and ending
				boolean bFound = false;
				for (int nStartIdx = 0; nStartIdx < arrlistPossibleStarting.size() && !bFound; nStartIdx += 1)
				{
					for (int nEndIdx = 0; nEndIdx < arrlistPossibleEnding.size() && !bFound; nEndIdx += 1)
					{
						// Check the Ending point is in a valid position. Should be in line or string length
						// from the starting point. If it is not we do not have to search
						int nColDiff = Math.abs(arrlistPossibleStarting.get(nStartIdx).m_nX - arrlistPossibleEnding.get(nEndIdx).m_nX);
						int nRowDiff = Math.abs(arrlistPossibleStarting.get(nStartIdx).m_nY - arrlistPossibleEnding.get(nEndIdx).m_nY);
						if (!((nColDiff == 0 || nColDiff == (m_listSearchWords.get(i).length() - 1))
							   && (nRowDiff == 0 || nRowDiff == (m_listSearchWords.get(i).length() - 1)))
							   || nRowDiff == 0 && nColDiff == 0)
						{
							// Invalid position
							continue;
						}

						// Set the steps for moving along the word
						int nColStep = (nColDiff == 0) ? 0 : 1;
						if (arrlistPossibleStarting.get(nStartIdx).m_nX > arrlistPossibleEnding.get(nEndIdx).m_nX)
						{
							// Going backwards
							nColStep = -1;
						}
						int nRowStep = (nRowDiff == 0) ? 0 : 1;
						if (arrlistPossibleStarting.get(nStartIdx).m_nY > arrlistPossibleEnding.get(nEndIdx).m_nY)
						{
							// Going backwards
							nRowStep = -1;
						}

						int nGridX = arrlistPossibleStarting.get(nStartIdx).m_nX + nColStep;
						int nGridY = arrlistPossibleStarting.get(nStartIdx).m_nY + nRowStep;

						bFound = true;
						// Check the letters inbetween the beginning and ending
						for (int nLength = 1; nLength < m_listSearchWords.get(i).length() - 1; nLength += 1)
						{
							if (m_carrSearchGrid[nGridX][nGridY] != m_listSearchWords.get(i).charAt(nLength))
							{
								// Chars did not match move to the next combination
								bFound = false;
								break;
							}
							
							nGridX += nColStep;
							nGridY += nRowStep;
						}

						if (bFound)
						{
							// Found the term log it
							arrlistMessages.add(String.format("%s %d:%d %d:%d", m_listSearchWords.get(i), arrlistPossibleStarting.get(nStartIdx).m_nY, arrlistPossibleStarting.get(nStartIdx).m_nX,
								   arrlistPossibleEnding.get(nEndIdx).m_nY, arrlistPossibleEnding.get(nEndIdx).m_nX));
						}
					}
				}
			}
		}
		catch (Exception _oX)
		{
		}

		return (arrlistMessages);
	}
}
