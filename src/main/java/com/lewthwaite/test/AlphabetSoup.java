package com.lewthwaite.test;

import java.io.File;
import java.util.List;

// Small program to check the contents of a grid word puzzzle.
//
//
//
//
//
//
//
public class AlphabetSoup
{

	public static void main(String[] _arrstrArgs)
	{
		// Enough arguments
		if (_arrstrArgs.length < 2)
		{
			// Invalid command line
			System.out.println("Error: Invalid command line");
			return;
		}

		// One input flag 
		// -i <file path>
		String strInputPath = null;
		try
		{
			for (int i = 0; i < _arrstrArgs.length; i += 1)
			{
				// Have we the input flag
				if (_arrstrArgs[i].equals("-i"))
				{
					// Load the file path
					strInputPath = _arrstrArgs[i + 1];
				}
			}
		} catch (Exception _oX)
		{
			// Invalid command line
			System.out.println("Error: Invalid command line");
			return;
		}

		if (strInputPath == null)
		{
			// No input path bail
			System.out.println("Error: No input path specified");
			return;
		}

		// Open the file 
		File fileInput = new File(strInputPath);
		if (!fileInput.exists())
		{
			// No input path bail
			System.out.println("Error: File does not exist.");
			return;
		}

		try
		{
			CLetterGrid oGrid = new CLetterGrid();
			
			if ( !oGrid.Load(fileInput))
			{
				// No input path bail
				System.out.println("Error: File does not exist.");
				return;
			}

			List<String> arrlistMessages = oGrid.CheckSearchTerms();
			
			// Output any hits 
			for ( int i = 0; i < arrlistMessages.size(); i += 1 )
			{
				System.out.println("Error: File does not exist.");
			}
		} 
		catch (Exception _oX)
		{
			// Error
			System.out.println("Error: Unable to process the input file.");
			return;
		}

	}
}
